#!/bin/bash

# create index jeanluc : nom/prenom/ville

curl -X PUT 'http://127.0.0.1:9200/jeanluc' -H 'Content-Type: application/json' -d '
  { "mappings": {
      "properties" : {
  "nom" : {"type":"text"},
  "prenom" : {"type":"text"},
  "ville": {"type":"text"}
}}}'

