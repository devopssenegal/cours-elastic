#!/bin/bash

# insert data index jeanluc / type users : nom/prenom/ville

curl -X POST 'http://localhost:9200/_bulk' -H 'Content-Type: application/json' -d '
{ "create" : { "_index" : "jeanluc", "_id": 1 } }
{"nom": "xavier", "prenom": "blog", "ville" : "Caen"}
{ "create" : { "_index" : "jeanluc", "_id": 2 } }
{"nom": "Michu", "prenom": "Paul", "ville" : "Lyon"}
{ "create" : { "_index" : "jeanluc", "_id": 3 } }
{"nom": "Michalon", "prenom": "Pierre", "ville" : "Marseille"}
'
